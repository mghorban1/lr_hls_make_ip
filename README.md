This package implements, tests and creates IP-Core of High-Level Synthesis (HLS) design of Linear-Regression (LR) algorithm:

TOP-LEVEL function:

    void LRHLS_top(hls::stream<LRStub>& strm_in, hls::stream<LRStub>& strm_out, const uint9_t& strm_len);

To run the code in this repository a version of Vivado must be installed on your machine. 

The link to install Xilinx products:

    https://www.xilinx.com/support/download.html 

Command below will allow running Vivado tools in command line and GUI mode.

    source <Vivado_Install_Dir>/Xilinx/Vivado/201x.x/settings64.sh
    
**SETTING THE ENVIRONMENT:**

To run the algorithm successfully, two repositories must be cloned in a directory, locally.

GitLab repository 'lr_hls_make_ip':

    git clone https://gitlab.cern.ch/mghorban1/lr_hls_make_ip.git

GitLab repository 'TrackerTFP':

    git clone https://gitlab.cern.ch/mghorban1/TrackerTFP.git

**The helper 'lr_hls_make_ip' repository contains:**

1) lr_hls_make_ip/hls:

- The add_links file, which creates a working directory for LR-HLS and links all the necessary files from CMSSW_x_x_x environment by creating symbolic links to the files.

- The vivado_hls_script.tcl to change HLS and FPGA parameters if required.

- Vivado_hls_directives.tcl to link the directives used in the implementation of LR-HLS. (optional)

- A testbench.cc file to test the LR-HLS design. 

- The result.golden.dat file which is used to compare the generated and expected results by creating or overwriting the result.dat during LR-HLS run.

- The data_in.txt which contains the test-vectors used to test the LR-HLS algorithm.

2) lr_hls_make_ip/hdl:

- A top.vhd wrapper for Vivado project in VHDL.

- A tb.vhd contains the test-vectors for testbenching the design.

- The vivado_hdl_script.tcl to specify and edit Vivado commands and FPGA board.

- A top.xdc to change the clock frequency, if necessary.

**The helper 'TrackerTFP' repository contains:**

1) TrackerTFP/

- All the CMSSW files needed for TrackerTFP design.

2) TrackerTFP/interface/HLS:

- The header files for LR-HLS design.

3) TrackerTFP/src/HLS:

- The source files for LR-HLS design.

Note: The execution of the code is compatible with CMSSW 11 version.

**THE CMSSW SECTION OF THE ALGORITHM:**

1) Create a directory:

        mkdir play/
        
2) Set the environment for CMSSW:
    
        /cvmfs/cms.cern.ch/cmsset_default.sh

3) Copy CMSSW_x_x_x in local machine and set the environment:

        cmsrel CMSSW_11_1_0_pre1
        cd src/
        cmsenv

4) Set $CMSSW used in add_links script to create symbolic links between LR-HLS and CMSSW: 

        export $CMSSW=/.../CMSSW_11_1_0_pre1/src
        
5) Git instructions:

        git-cms-init
        git remote add maziar https://github.com/mzrghorbani/cmssw.git
        git checkout maziar

6) Adding CMSSW git packages:

        git cms-addpkg L1Trigger/TrackerDTC DataFormats/L1TrackTrigger SimTracker/TrackTriggerAssociation

7) Change the directory and copy the TrackerTFP repository:

        cd L1Trigger/
        git clone https://gitlab.cern.ch/mghorban1/TrackeTFP.git

**THE LR-HLS SECTION OF THE ALGORITHM:**

1) Copy the lr_hls_make_ip repository:

        cd play/
        git clone https://gitlab.cern.ch/mghorban1/lr_hls_make_ip.git
        
**CONNECTING ALL DOTS:**

1) Setting the CMSSW environment for LR-HLS algorithm:

        cd lr_hls_make_ip/hls/
        chmod +x add_links
        ./add_links
        
2) Ensure all the necessary files are linked:

        cd workspace/

3) The Vivado command below runs the LR-HLS design using the files in TrackerTFP and lr_hls_make_ip directories:

        vivado_hls -f vivado_hls_script.tcl

After the command is executed successfully, the reports can be accessed via 'LRHLS/solution1' subdirectories. 

Note: To view the generated waveform in Vivado Simulator use the Waveform Configuration File (.wcfg) in 'LRHLS/solution_1/sim'.

If modelsim is preferred the Wave Log Format (.wlf) file must be generated first by modifying the vivado_hls_script.tcl script by adding modelsim to cosim_design command:

    cosim_design -trace_level all -rtl vhdl -tool modelsim   

Optionally, open Vivado_hls GUI with the command;

    vivado_hls -p LRHLS
    
CREATING VIVADO PROJECT WITH LR-HLS IP-CORE:

    cd hdl/
    vivado -mode batch -source vivado_hdl_script.tcl

Optionally, open Vivado GUI with the command;

    vivado hdl/LR_useHLS_IP/LR_useHLS_IP.xpr 
    
Change the directory for CMSSW simulations:

    cd play/CMSSW_11_1_0_pre1/src
    cmsenv
    scram b -j4
    cmsRun L1Trigger/TrackerTFP/test/demo_cfg.py
    
The reports are displayed following the execution of above commands.

--- Contact email: 
    Maziar.Ghorbani@brunel.ac.uk