library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity tb is
--  Port ( );
end tb;

architecture behavioral of tb is
component top is
    port (
    ap_ready : out STD_LOGIC;
    ap_clk : in STD_LOGIC;
    ap_start : in STD_LOGIC;
    ap_rst : in STD_LOGIC;
    ap_idle : out STD_LOGIC;
    ap_done : out STD_LOGIC;
    strm_len_V : in STD_LOGIC_VECTOR ( 9 downto 0 );
    strm_in_V_r_V_rd_data : in STD_LOGIC_VECTOR ( 13 downto 0 );
    strm_in_V_r_V_empty_n : in STD_LOGIC;
    strm_in_V_r_V_rd_en : out STD_LOGIC;
    strm_in_V_barrel_V_rd_data : in STD_LOGIC_VECTOR ( 0 to 0 );
    strm_in_V_barrel_V_empty_n : in STD_LOGIC;
    strm_in_V_barrel_V_rd_en : out STD_LOGIC;
    strm_in_V_z_V_rd_data : in STD_LOGIC_VECTOR ( 13 downto 0 );
    strm_in_V_z_V_empty_n : in STD_LOGIC;
    strm_in_V_z_V_rd_en : out STD_LOGIC;
    strm_in_V_layer_V_rd_data : in STD_LOGIC_VECTOR ( 2 downto 0 );
    strm_in_V_layer_V_empty_n : in STD_LOGIC;
    strm_in_V_layer_V_rd_en : out STD_LOGIC;
    strm_in_V_phi_V_rd_data : in STD_LOGIC_VECTOR ( 13 downto 0 );
    strm_in_V_phi_V_empty_n : in STD_LOGIC;
    strm_in_V_phi_V_rd_en : out STD_LOGIC;
    strm_in_V_psModule_V_rd_data : in STD_LOGIC_VECTOR ( 0 to 0 );
    strm_in_V_psModule_V_empty_n : in STD_LOGIC;
    strm_in_V_psModule_V_rd_en : out STD_LOGIC;
    strm_in_V_valid_V_rd_data : in STD_LOGIC_VECTOR ( 0 to 0 );
    strm_in_V_valid_V_empty_n : in STD_LOGIC;
    strm_in_V_valid_V_rd_en : out STD_LOGIC;
    agg_result_zT_V_ap_vld : out STD_LOGIC;
    agg_result_qOverPt_V_ap_vld : out STD_LOGIC;
    agg_result_zT_V : out STD_LOGIC_VECTOR ( 13 downto 0 );
    agg_result_qOverPt_V : out STD_LOGIC_VECTOR ( 13 downto 0 );
    agg_result_cot_V_ap_vld : out STD_LOGIC;
    agg_result_phiT_V : out STD_LOGIC_VECTOR ( 13 downto 0 );
    agg_result_phiT_V_ap_vld : out STD_LOGIC;
    agg_result_cot_V : out STD_LOGIC_VECTOR ( 13 downto 0 );
    strm_out_V_psModule_V_wr_data : out STD_LOGIC_VECTOR ( 0 to 0 );
    strm_out_V_psModule_V_full_n : in STD_LOGIC;
    strm_out_V_psModule_V_wr_en : out STD_LOGIC;
    strm_out_V_r_V_wr_data : out STD_LOGIC_VECTOR ( 13 downto 0 );
    strm_out_V_r_V_full_n : in STD_LOGIC;
    strm_out_V_r_V_wr_en : out STD_LOGIC;
    strm_out_V_barrel_V_wr_data : out STD_LOGIC_VECTOR ( 0 to 0 );
    strm_out_V_barrel_V_full_n : in STD_LOGIC;
    strm_out_V_barrel_V_wr_en : out STD_LOGIC;
    strm_out_V_valid_V_wr_data : out STD_LOGIC_VECTOR ( 0 to 0 );
    strm_out_V_valid_V_full_n : in STD_LOGIC;
    strm_out_V_valid_V_wr_en : out STD_LOGIC;
    strm_out_V_z_V_wr_data : out STD_LOGIC_VECTOR ( 13 downto 0 );
    strm_out_V_z_V_full_n : in STD_LOGIC;
    strm_out_V_z_V_wr_en : out STD_LOGIC;
    strm_out_V_layer_V_wr_data : out STD_LOGIC_VECTOR ( 2 downto 0 );
    strm_out_V_layer_V_full_n : in STD_LOGIC;
    strm_out_V_layer_V_wr_en : out STD_LOGIC;
    strm_out_V_phi_V_wr_data : out STD_LOGIC_VECTOR ( 13 downto 0 );
    strm_out_V_phi_V_full_n : in STD_LOGIC;
    strm_out_V_phi_V_wr_en : out STD_LOGIC
  );
  end component top;

  constant T : time := 2.778 ns;
  signal ap_clk : STD_LOGIC;
  signal ap_rst : STD_LOGIC;
  signal ap_start : STD_LOGIC;
  signal ap_done : STD_LOGIC;
  signal ap_idle : STD_LOGIC;
  signal ap_ready : STD_LOGIC;
  signal strm_len_V : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal agg_result_cot_V : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal agg_result_cot_V_ap_vld : STD_LOGIC;
  signal agg_result_phiT_V : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal agg_result_phiT_V_ap_vld : STD_LOGIC;
  signal agg_result_qOverPt_V : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal agg_result_qOverPt_V_ap_vld : STD_LOGIC;
  signal agg_result_zT_V : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal agg_result_zT_V_ap_vld : STD_LOGIC;
  signal strm_in_V_barrel_V_EMPTY_N : STD_LOGIC;
  signal strm_in_V_barrel_V_RD_DATA : STD_LOGIC_VECTOR ( 0 to 0 );
  signal strm_in_V_barrel_V_RD_EN : STD_LOGIC;
  signal strm_in_V_layer_V_EMPTY_N : STD_LOGIC;
  signal strm_in_V_layer_V_RD_DATA : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal strm_in_V_layer_V_RD_EN : STD_LOGIC;
  signal strm_in_V_phi_V_EMPTY_N : STD_LOGIC;
  signal strm_in_V_phi_V_RD_DATA : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal strm_in_V_phi_V_RD_EN : STD_LOGIC;
  signal strm_in_V_psModule_V_EMPTY_N : STD_LOGIC;
  signal strm_in_V_psModule_V_RD_DATA : STD_LOGIC_VECTOR ( 0 to 0 );
  signal strm_in_V_psModule_V_RD_EN : STD_LOGIC;
  signal strm_in_V_r_V_EMPTY_N : STD_LOGIC;
  signal strm_in_V_r_V_RD_DATA : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal strm_in_V_r_V_RD_EN : STD_LOGIC;
  signal strm_in_V_valid_V_EMPTY_N : STD_LOGIC;
  signal strm_in_V_valid_V_RD_DATA : STD_LOGIC_VECTOR ( 0 to 0 );
  signal strm_in_V_valid_V_RD_EN : STD_LOGIC;
  signal strm_in_V_z_V_EMPTY_N : STD_LOGIC;
  signal strm_in_V_z_V_RD_DATA : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal strm_in_V_z_V_RD_EN : STD_LOGIC;
  signal strm_out_V_barrel_V_FULL_N : STD_LOGIC;
  signal strm_out_V_barrel_V_WR_DATA : STD_LOGIC_VECTOR ( 0 to 0 );
  signal strm_out_V_barrel_V_WR_EN : STD_LOGIC;
  signal strm_out_V_layer_V_FULL_N : STD_LOGIC;
  signal strm_out_V_layer_V_WR_DATA : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal strm_out_V_layer_V_WR_EN : STD_LOGIC;
  signal strm_out_V_phi_V_FULL_N : STD_LOGIC;
  signal strm_out_V_phi_V_WR_DATA : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal strm_out_V_phi_V_WR_EN : STD_LOGIC;
  signal strm_out_V_psModule_V_FULL_N : STD_LOGIC;
  signal strm_out_V_psModule_V_WR_DATA : STD_LOGIC_VECTOR ( 0 to 0 );
  signal strm_out_V_psModule_V_WR_EN : STD_LOGIC;
  signal strm_out_V_r_V_FULL_N : STD_LOGIC;
  signal strm_out_V_r_V_WR_DATA : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal strm_out_V_r_V_WR_EN : STD_LOGIC;
  signal strm_out_V_valid_V_FULL_N : STD_LOGIC;
  signal strm_out_V_valid_V_WR_DATA : STD_LOGIC_VECTOR ( 0 to 0 );
  signal strm_out_V_valid_V_WR_EN : STD_LOGIC;
  signal strm_out_V_z_V_FULL_N : STD_LOGIC;
  signal strm_out_V_z_V_WR_DATA : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal strm_out_V_z_V_WR_EN : STD_LOGIC;

  begin

  top_0: component top
	port map (
	  ap_clk => ap_clk,
      ap_done => ap_done,
      ap_idle => ap_idle,
      ap_ready => ap_ready,
      ap_rst => ap_rst,
      ap_start => ap_start,
      agg_result_cot_V(13 downto 0) => agg_result_cot_V(13 downto 0),
      agg_result_cot_V_ap_vld => agg_result_cot_V_ap_vld,
      agg_result_phiT_V(13 downto 0) => agg_result_phiT_V(13 downto 0),
      agg_result_phiT_V_ap_vld => agg_result_phiT_V_ap_vld,
      agg_result_qOverPt_V(13 downto 0) => agg_result_qOverPt_V(13 downto 0),
      agg_result_qOverPt_V_ap_vld => agg_result_qOverPt_V_ap_vld,
      agg_result_zT_V(13 downto 0) => agg_result_zT_V(13 downto 0),
      agg_result_zT_V_ap_vld => agg_result_zT_V_ap_vld,
      strm_in_V_barrel_V_empty_n => strm_in_V_barrel_V_empty_n,
      strm_in_V_barrel_V_rd_data(0) => strm_in_V_barrel_V_rd_data(0),
      strm_in_V_barrel_V_rd_en => strm_in_V_barrel_V_rd_en,
      strm_in_V_layer_V_empty_n => strm_in_V_layer_V_empty_n,
      strm_in_V_layer_V_rd_data(2 downto 0) => strm_in_V_layer_V_rd_data(2 downto 0),
      strm_in_V_layer_V_rd_en => strm_in_V_layer_V_rd_en,
      strm_in_V_phi_V_empty_n => strm_in_V_phi_V_empty_n,
      strm_in_V_phi_V_rd_data(13 downto 0) => strm_in_V_phi_V_rd_data(13 downto 0),
      strm_in_V_phi_V_rd_en => strm_in_V_phi_V_rd_en,
      strm_in_V_psModule_V_empty_n => strm_in_V_psModule_V_empty_n,
      strm_in_V_psModule_V_rd_data(0) => strm_in_V_psModule_V_rd_data(0),
      strm_in_V_psModule_V_rd_en => strm_in_V_psModule_V_rd_en,
      strm_in_V_r_V_empty_n => strm_in_V_r_V_empty_n,
      strm_in_V_r_V_rd_data(13 downto 0) => strm_in_V_r_V_rd_data(13 downto 0),
      strm_in_V_r_V_rd_en => strm_in_V_r_V_rd_en,
      strm_in_V_valid_V_empty_n => strm_in_V_valid_V_empty_n,
      strm_in_V_valid_V_rd_data(0) => strm_in_V_valid_V_rd_data(0),
      strm_in_V_valid_V_rd_en => strm_in_V_valid_V_rd_en,
      strm_in_V_z_V_empty_n => strm_in_V_z_V_empty_n,
      strm_in_V_z_V_rd_data(13 downto 0) => strm_in_V_z_V_rd_data(13 downto 0),
      strm_in_V_z_V_rd_en => strm_in_V_z_V_rd_en,
      strm_len_V(9 downto 0) => strm_len_V(9 downto 0),
      strm_out_V_barrel_V_full_n => strm_out_V_barrel_V_full_n,
      strm_out_V_barrel_V_wr_data(0) => strm_out_V_barrel_V_wr_data(0),
      strm_out_V_barrel_V_wr_en => strm_out_V_barrel_V_wr_en,
      strm_out_V_layer_V_full_n => strm_out_V_layer_V_full_n,
      strm_out_V_layer_V_wr_data(2 downto 0) => strm_out_V_layer_V_wr_data(2 downto 0),
      strm_out_V_layer_V_wr_en => strm_out_V_layer_V_wr_en,
      strm_out_V_phi_V_full_n => strm_out_V_phi_V_full_n,
      strm_out_V_phi_V_wr_data(13 downto 0) => strm_out_V_phi_V_wr_data(13 downto 0),
      strm_out_V_phi_V_wr_en => strm_out_V_phi_V_wr_en,
      strm_out_V_psModule_V_full_n => strm_out_V_psModule_V_full_n,
      strm_out_V_psModule_V_wr_data(0) => strm_out_V_psModule_V_wr_data(0),
      strm_out_V_psModule_V_wr_en => strm_out_V_psModule_V_wr_en,
      strm_out_V_r_V_full_n => strm_out_V_r_V_full_n,
      strm_out_V_r_V_wr_data(13 downto 0) => strm_out_V_r_V_wr_data(13 downto 0),
      strm_out_V_r_V_wr_en => strm_out_V_r_V_wr_en,
      strm_out_V_valid_V_full_n => strm_out_V_valid_V_full_n,
      strm_out_V_valid_V_wr_data(0) => strm_out_V_valid_V_wr_data(0),
      strm_out_V_valid_V_wr_en => strm_out_V_valid_V_wr_en,
      strm_out_V_z_V_full_n => strm_out_V_z_V_full_n,
      strm_out_V_z_V_wr_data(13 downto 0) => strm_out_V_z_V_wr_data(13 downto 0),
      strm_out_V_z_V_wr_en => strm_out_V_z_V_wr_en
    );

    clk_proc: process 
    begin
        ap_clk <= '1';
        wait for T/2;
        ap_clk <= '0';
        wait for T/2;
    end process;

    stimula_proc: process
    begin
    	-- begin

    	-- end
    	wait;
	end process;

end behavioral;