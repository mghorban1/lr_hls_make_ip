/**                                                                                                                   
 * Author by Maziar Ghorbani - Brunel University London                                                               
 * Test-bench for Class LR-HLS
 * Based on implementation of Linear-Regression algorithm by Dr Thomas Schuh
 */

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "LRHLS_top.h"

// Vector variables declarations to hold stub parameters form text.csv file
typedef std::vector<double> record_t;
typedef std::vector<record_t> data_t;

// Function to read stub parameters from text file
std::istream &operator>>(std::istream &ins, record_t &record) {
    record.clear();
    std::string line;
    getline(ins, line);
    std::stringstream ss(line);
    std::string field;
    while (getline(ss, field, ',')) {
        std::stringstream fs(field);
        double f;
        fs >> f;
        record.push_back(f);
    }
    return ins;
}

// Function to read stub parameters from text file
std::istream &operator>>(std::istream &ins, data_t &data) {
    data.clear();
    record_t record;
    while (ins >> record) {
        data.push_back(record);
    }
    return ins;
}

int main(int argc, char **argv) {

    data_t data;
//    std::ifstream infile("/home/mghorbani/workspace/git/lr_hls_make_ip/hls/workspace/data_in.txt");
    std::ifstream infile("data_in.txt");
    infile >> data;
    if (!infile.eof()) {
        std::cout << "ERROR: Can't open data_in.txt file\n";
        return -1;
    }
    infile.close();

    FILE *fp;
    fp = fopen("result.dat", "w");

    // Declarations and interfaces for hls::stream class
    strm_t strm_in;
    strm_t strm_out;

    // Passing stub parameters to struct LRStub
    for (uint9_t i = 0; i < data.size(); i++) {
        LRStub stub;
        stub.r = data[i][0];
        stub.phi = data[i][1];
        stub.z = data[i][2];
        stub.layer = data[i][3];
        stub.barrel = data[i][4];
        stub.psModule = data[i][5];
        stub.valid = 1;
        strm_in << stub;
#ifdef PRINT_SUMMARY
        double r_ld = data[i][0];
        double phi_ld = data[i][1];
        double z_ld = data[i][2];
        CHECK_AP::checkCalc("stub.r", stub.r , r_ld, 0.001);
        CHECK_AP::checkCalc("stub.phi", stub.phi , phi_ld, 0.001);
        CHECK_AP::checkCalc("stub.z", stub.z , z_ld, 0.001);
#endif
    }

    // hls::stream length for variable-sized data
    const uint9_t strm_len = strm_in.size();

    // Top function for calling LR-HLS class
    LRHLS_top(strm_in, strm_out, strm_in.size());

    // Reading back the stub parameters and print them for analysis
    for (uint9_t i = 0; i < strm_len; i++) {
        LRStub stub = strm_out.read();
        fprintf(fp, "%f,%f,%f,%d,%d,%d,%d\n",
                stub.r.to_double(),
                stub.phi.to_double(),
                stub.z.to_double(),
                stub.layer.to_uint(),
                stub.barrel.to_uint(),
                stub.psModule.to_uint(),
                stub.valid.to_uint());
    }

    fclose(fp);

    // Activate comparison after fixed-point design is completed
    /*
    printf("Comparing against output data \n");

    if (system("diff -w result.dat result.golden.dat")) {
        fprintf(stdout, "*********************************************\n");
        fprintf(stdout, "FAIL: Output DOES NOT match the golden output\n");
        fprintf(stdout, "*********************************************\n");
        return 1;
    } else {
        fprintf(stdout, "*******************************************\n");
        fprintf(stdout, "PASS: The output matches the golden output!\n");
        fprintf(stdout, "*******************************************\n");
        return 0;
    }
    */
    return 0;
}
