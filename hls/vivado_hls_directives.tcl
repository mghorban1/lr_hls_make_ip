############################################################
### This file is generated automatically by Vivado HLS.
### Please DO NOT edit it.
### Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
#############################################################
set_directive_interface -mode ap_none "LRHLS_top" strm_len
set_directive_pipeline "LRHLS_top"
set_directive_array_partition -type complete -dim 1 "LRHLS_top" stubs_in
set_directive_array_partition -type complete -dim 1 "LRHLS_top" lrhls.layerPopulation_
