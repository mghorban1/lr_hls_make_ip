############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
############################################################
delete_project LRHLS
open_project LRHLS
set_top LRHLS_top
add_files LRHLS_types.h -cflags "-std=c++11" -csimflags "-std=c++14"
add_files LRHLS_utility.h -cflags "-std=c++11" -csimflags "-std=c++14"
add_files LRHLS_top.h -cflags "-std=c++11" -csimflags "-std=c++14"
add_files LRHLS_top.cc -cflags "-std=c++11" -csimflags "-std=c++14"
add_files LRHLS.h -cflags "-std=c++11" -csimflags "-std=c++14"
add_files LRHLS.cc -cflags "-std=c++11" -csimflags "-std=c++14"
add_files -tb data_in.txt -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
add_files -tb LRHLS_test.cc -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
add_files -tb result.golden.dat -cflags "-Wno-unknown-pragmas" -csimflags "-Wno-unknown-pragmas"
open_solution "solution1"
set_part {xcvu9p-flgb2104-2-e}
create_clock -period 2.77 -name default
config_compile -name_max_length 100  -pipeline_loops 0 -unsafe_math_optimizations
config_export -display_name LRHLS_IP -format ip_catalog -library hls -rtl vhdl
source "./vivado_hls_directives.tcl"
csim_design
csynth_design
cosim_design -trace_level all -rtl vhdl
export_design -rtl vhdl -format ip_catalog -display_name "LRHLS_IP"
exit